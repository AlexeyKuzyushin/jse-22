package ru.rencredit.jschool.kuzyushin.tm.command.data.base64;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Role;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Session;

public final class DataBase64LoadCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "data-base64-load";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from base64 file";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA BINARY LOAD]");
        if (serviceLocator != null) {
            @Nullable final Session session = serviceLocator.getSessionService().getCurrentSession();
            serviceLocator.getDataEndpoint().loadDataBase64(session);
            System.out.println("[OK]");
        }
        else System.out.println("[FAILED]");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }
}
