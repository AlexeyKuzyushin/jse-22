package ru.rencredit.jschool.kuzyushin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.util.NumberUtil;

public final class SystemInfoCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "info";
    }

    @NotNull
    @Override
    public String arg() {
        return "-i";
    }

    @NotNull
    @Override
    public String description() {
        return "Show system info";
    }

    @Override
    public void execute() {
        System.out.println("[INFO]");

        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + processors);

        final long freeMemory = Runtime.getRuntime().freeMemory();
        @NotNull final String freeMemoryFormat = NumberUtil.formatBytes(freeMemory);
        System.out.println("Free memory: " + freeMemoryFormat);

        final long maxMemory = Runtime.getRuntime().maxMemory();
        @NotNull final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        @NotNull final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);

        final long totalMemory = Runtime.getRuntime().totalMemory();
        @NotNull final String totalMemoryFormat = NumberUtil.formatBytes(totalMemory);
        System.out.println("Total memory available to JVM: " + totalMemoryFormat);

        final long usedMemory = totalMemory - freeMemory;
        @NotNull final String usedMemoryFormat = NumberUtil.formatBytes(usedMemory);
        System.out.println("Used memory by JVM: " + usedMemoryFormat);
    }
}
