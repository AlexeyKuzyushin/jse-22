package ru.rencredit.jschool.kuzyushin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Session;

public final class TaskClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "task-clear";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CLEAR TASKS]");
        if (serviceLocator != null) {
            @Nullable final Session session = serviceLocator.getSessionService().getCurrentSession();
            serviceLocator.getTaskEndpoint().clearTasks(session);
            System.out.println("[OK]");
        }
        else System.out.println("[FAILED]");
    }
}
