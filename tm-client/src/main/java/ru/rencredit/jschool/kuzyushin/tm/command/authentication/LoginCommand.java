package ru.rencredit.jschool.kuzyushin.tm.command.authentication;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Session;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

public final class LoginCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "login";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Login";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOGIN]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        if (serviceLocator != null) {
            @Nullable final Session session = serviceLocator.getSessionEndpoint().openSession(login, password);
            serviceLocator.getSessionService().setCurrentSession(session);
        }
        System.out.println("[OK]");
    }
}
