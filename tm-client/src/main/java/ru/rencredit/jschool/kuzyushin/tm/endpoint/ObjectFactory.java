
package ru.rencredit.jschool.kuzyushin.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.rencredit.jschool.kuzyushin.tm.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Exception_QNAME = new QName("http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", "Exception");
    private final static QName _ClearDataBase64_QNAME = new QName("http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", "clearDataBase64");
    private final static QName _ClearDataBase64Response_QNAME = new QName("http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", "clearDataBase64Response");
    private final static QName _ClearDataBinary_QNAME = new QName("http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", "clearDataBinary");
    private final static QName _ClearDataBinaryResponse_QNAME = new QName("http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", "clearDataBinaryResponse");
    private final static QName _ClearDataJson_QNAME = new QName("http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", "clearDataJson");
    private final static QName _ClearDataJsonResponse_QNAME = new QName("http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", "clearDataJsonResponse");
    private final static QName _ClearDataXml_QNAME = new QName("http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", "clearDataXml");
    private final static QName _ClearDataXmlResponse_QNAME = new QName("http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", "clearDataXmlResponse");
    private final static QName _LoadDataBase64_QNAME = new QName("http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", "loadDataBase64");
    private final static QName _LoadDataBase64Response_QNAME = new QName("http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", "loadDataBase64Response");
    private final static QName _LoadDataBinary_QNAME = new QName("http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", "loadDataBinary");
    private final static QName _LoadDataBinaryResponse_QNAME = new QName("http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", "loadDataBinaryResponse");
    private final static QName _LoadDataJson_QNAME = new QName("http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", "loadDataJson");
    private final static QName _LoadDataJsonResponse_QNAME = new QName("http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", "loadDataJsonResponse");
    private final static QName _LoadDataXml_QNAME = new QName("http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", "loadDataXml");
    private final static QName _LoadDataXmlResponse_QNAME = new QName("http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", "loadDataXmlResponse");
    private final static QName _SaveDataBase64_QNAME = new QName("http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", "saveDataBase64");
    private final static QName _SaveDataBase64Response_QNAME = new QName("http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", "saveDataBase64Response");
    private final static QName _SaveDataBinary_QNAME = new QName("http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", "saveDataBinary");
    private final static QName _SaveDataBinaryResponse_QNAME = new QName("http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", "saveDataBinaryResponse");
    private final static QName _SaveDataJson_QNAME = new QName("http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", "saveDataJson");
    private final static QName _SaveDataJsonResponse_QNAME = new QName("http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", "saveDataJsonResponse");
    private final static QName _SaveDataXml_QNAME = new QName("http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", "saveDataXml");
    private final static QName _SaveDataXmlResponse_QNAME = new QName("http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", "saveDataXmlResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.rencredit.jschool.kuzyushin.tm.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link ClearDataBase64 }
     * 
     */
    public ClearDataBase64 createClearDataBase64() {
        return new ClearDataBase64();
    }

    /**
     * Create an instance of {@link ClearDataBase64Response }
     * 
     */
    public ClearDataBase64Response createClearDataBase64Response() {
        return new ClearDataBase64Response();
    }

    /**
     * Create an instance of {@link ClearDataBinary }
     * 
     */
    public ClearDataBinary createClearDataBinary() {
        return new ClearDataBinary();
    }

    /**
     * Create an instance of {@link ClearDataBinaryResponse }
     * 
     */
    public ClearDataBinaryResponse createClearDataBinaryResponse() {
        return new ClearDataBinaryResponse();
    }

    /**
     * Create an instance of {@link ClearDataJson }
     * 
     */
    public ClearDataJson createClearDataJson() {
        return new ClearDataJson();
    }

    /**
     * Create an instance of {@link ClearDataJsonResponse }
     * 
     */
    public ClearDataJsonResponse createClearDataJsonResponse() {
        return new ClearDataJsonResponse();
    }

    /**
     * Create an instance of {@link ClearDataXml }
     * 
     */
    public ClearDataXml createClearDataXml() {
        return new ClearDataXml();
    }

    /**
     * Create an instance of {@link ClearDataXmlResponse }
     * 
     */
    public ClearDataXmlResponse createClearDataXmlResponse() {
        return new ClearDataXmlResponse();
    }

    /**
     * Create an instance of {@link LoadDataBase64 }
     * 
     */
    public LoadDataBase64 createLoadDataBase64() {
        return new LoadDataBase64();
    }

    /**
     * Create an instance of {@link LoadDataBase64Response }
     * 
     */
    public LoadDataBase64Response createLoadDataBase64Response() {
        return new LoadDataBase64Response();
    }

    /**
     * Create an instance of {@link LoadDataBinary }
     * 
     */
    public LoadDataBinary createLoadDataBinary() {
        return new LoadDataBinary();
    }

    /**
     * Create an instance of {@link LoadDataBinaryResponse }
     * 
     */
    public LoadDataBinaryResponse createLoadDataBinaryResponse() {
        return new LoadDataBinaryResponse();
    }

    /**
     * Create an instance of {@link LoadDataJson }
     * 
     */
    public LoadDataJson createLoadDataJson() {
        return new LoadDataJson();
    }

    /**
     * Create an instance of {@link LoadDataJsonResponse }
     * 
     */
    public LoadDataJsonResponse createLoadDataJsonResponse() {
        return new LoadDataJsonResponse();
    }

    /**
     * Create an instance of {@link LoadDataXml }
     * 
     */
    public LoadDataXml createLoadDataXml() {
        return new LoadDataXml();
    }

    /**
     * Create an instance of {@link LoadDataXmlResponse }
     * 
     */
    public LoadDataXmlResponse createLoadDataXmlResponse() {
        return new LoadDataXmlResponse();
    }

    /**
     * Create an instance of {@link SaveDataBase64 }
     * 
     */
    public SaveDataBase64 createSaveDataBase64() {
        return new SaveDataBase64();
    }

    /**
     * Create an instance of {@link SaveDataBase64Response }
     * 
     */
    public SaveDataBase64Response createSaveDataBase64Response() {
        return new SaveDataBase64Response();
    }

    /**
     * Create an instance of {@link SaveDataBinary }
     * 
     */
    public SaveDataBinary createSaveDataBinary() {
        return new SaveDataBinary();
    }

    /**
     * Create an instance of {@link SaveDataBinaryResponse }
     * 
     */
    public SaveDataBinaryResponse createSaveDataBinaryResponse() {
        return new SaveDataBinaryResponse();
    }

    /**
     * Create an instance of {@link SaveDataJson }
     * 
     */
    public SaveDataJson createSaveDataJson() {
        return new SaveDataJson();
    }

    /**
     * Create an instance of {@link SaveDataJsonResponse }
     * 
     */
    public SaveDataJsonResponse createSaveDataJsonResponse() {
        return new SaveDataJsonResponse();
    }

    /**
     * Create an instance of {@link SaveDataXml }
     * 
     */
    public SaveDataXml createSaveDataXml() {
        return new SaveDataXml();
    }

    /**
     * Create an instance of {@link SaveDataXmlResponse }
     * 
     */
    public SaveDataXmlResponse createSaveDataXmlResponse() {
        return new SaveDataXmlResponse();
    }

    /**
     * Create an instance of {@link Session }
     * 
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearDataBase64 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", name = "clearDataBase64")
    public JAXBElement<ClearDataBase64> createClearDataBase64(ClearDataBase64 value) {
        return new JAXBElement<ClearDataBase64>(_ClearDataBase64_QNAME, ClearDataBase64 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearDataBase64Response }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", name = "clearDataBase64Response")
    public JAXBElement<ClearDataBase64Response> createClearDataBase64Response(ClearDataBase64Response value) {
        return new JAXBElement<ClearDataBase64Response>(_ClearDataBase64Response_QNAME, ClearDataBase64Response.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearDataBinary }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", name = "clearDataBinary")
    public JAXBElement<ClearDataBinary> createClearDataBinary(ClearDataBinary value) {
        return new JAXBElement<ClearDataBinary>(_ClearDataBinary_QNAME, ClearDataBinary.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearDataBinaryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", name = "clearDataBinaryResponse")
    public JAXBElement<ClearDataBinaryResponse> createClearDataBinaryResponse(ClearDataBinaryResponse value) {
        return new JAXBElement<ClearDataBinaryResponse>(_ClearDataBinaryResponse_QNAME, ClearDataBinaryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearDataJson }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", name = "clearDataJson")
    public JAXBElement<ClearDataJson> createClearDataJson(ClearDataJson value) {
        return new JAXBElement<ClearDataJson>(_ClearDataJson_QNAME, ClearDataJson.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearDataJsonResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", name = "clearDataJsonResponse")
    public JAXBElement<ClearDataJsonResponse> createClearDataJsonResponse(ClearDataJsonResponse value) {
        return new JAXBElement<ClearDataJsonResponse>(_ClearDataJsonResponse_QNAME, ClearDataJsonResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearDataXml }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", name = "clearDataXml")
    public JAXBElement<ClearDataXml> createClearDataXml(ClearDataXml value) {
        return new JAXBElement<ClearDataXml>(_ClearDataXml_QNAME, ClearDataXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClearDataXmlResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", name = "clearDataXmlResponse")
    public JAXBElement<ClearDataXmlResponse> createClearDataXmlResponse(ClearDataXmlResponse value) {
        return new JAXBElement<ClearDataXmlResponse>(_ClearDataXmlResponse_QNAME, ClearDataXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataBase64 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", name = "loadDataBase64")
    public JAXBElement<LoadDataBase64> createLoadDataBase64(LoadDataBase64 value) {
        return new JAXBElement<LoadDataBase64>(_LoadDataBase64_QNAME, LoadDataBase64 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataBase64Response }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", name = "loadDataBase64Response")
    public JAXBElement<LoadDataBase64Response> createLoadDataBase64Response(LoadDataBase64Response value) {
        return new JAXBElement<LoadDataBase64Response>(_LoadDataBase64Response_QNAME, LoadDataBase64Response.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataBinary }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", name = "loadDataBinary")
    public JAXBElement<LoadDataBinary> createLoadDataBinary(LoadDataBinary value) {
        return new JAXBElement<LoadDataBinary>(_LoadDataBinary_QNAME, LoadDataBinary.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataBinaryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", name = "loadDataBinaryResponse")
    public JAXBElement<LoadDataBinaryResponse> createLoadDataBinaryResponse(LoadDataBinaryResponse value) {
        return new JAXBElement<LoadDataBinaryResponse>(_LoadDataBinaryResponse_QNAME, LoadDataBinaryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataJson }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", name = "loadDataJson")
    public JAXBElement<LoadDataJson> createLoadDataJson(LoadDataJson value) {
        return new JAXBElement<LoadDataJson>(_LoadDataJson_QNAME, LoadDataJson.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataJsonResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", name = "loadDataJsonResponse")
    public JAXBElement<LoadDataJsonResponse> createLoadDataJsonResponse(LoadDataJsonResponse value) {
        return new JAXBElement<LoadDataJsonResponse>(_LoadDataJsonResponse_QNAME, LoadDataJsonResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataXml }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", name = "loadDataXml")
    public JAXBElement<LoadDataXml> createLoadDataXml(LoadDataXml value) {
        return new JAXBElement<LoadDataXml>(_LoadDataXml_QNAME, LoadDataXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDataXmlResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", name = "loadDataXmlResponse")
    public JAXBElement<LoadDataXmlResponse> createLoadDataXmlResponse(LoadDataXmlResponse value) {
        return new JAXBElement<LoadDataXmlResponse>(_LoadDataXmlResponse_QNAME, LoadDataXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataBase64 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", name = "saveDataBase64")
    public JAXBElement<SaveDataBase64> createSaveDataBase64(SaveDataBase64 value) {
        return new JAXBElement<SaveDataBase64>(_SaveDataBase64_QNAME, SaveDataBase64 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataBase64Response }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", name = "saveDataBase64Response")
    public JAXBElement<SaveDataBase64Response> createSaveDataBase64Response(SaveDataBase64Response value) {
        return new JAXBElement<SaveDataBase64Response>(_SaveDataBase64Response_QNAME, SaveDataBase64Response.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataBinary }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", name = "saveDataBinary")
    public JAXBElement<SaveDataBinary> createSaveDataBinary(SaveDataBinary value) {
        return new JAXBElement<SaveDataBinary>(_SaveDataBinary_QNAME, SaveDataBinary.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataBinaryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", name = "saveDataBinaryResponse")
    public JAXBElement<SaveDataBinaryResponse> createSaveDataBinaryResponse(SaveDataBinaryResponse value) {
        return new JAXBElement<SaveDataBinaryResponse>(_SaveDataBinaryResponse_QNAME, SaveDataBinaryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataJson }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", name = "saveDataJson")
    public JAXBElement<SaveDataJson> createSaveDataJson(SaveDataJson value) {
        return new JAXBElement<SaveDataJson>(_SaveDataJson_QNAME, SaveDataJson.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataJsonResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", name = "saveDataJsonResponse")
    public JAXBElement<SaveDataJsonResponse> createSaveDataJsonResponse(SaveDataJsonResponse value) {
        return new JAXBElement<SaveDataJsonResponse>(_SaveDataJsonResponse_QNAME, SaveDataJsonResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataXml }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", name = "saveDataXml")
    public JAXBElement<SaveDataXml> createSaveDataXml(SaveDataXml value) {
        return new JAXBElement<SaveDataXml>(_SaveDataXml_QNAME, SaveDataXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDataXmlResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.kuzyushin.jschool.rencredit.ru/", name = "saveDataXmlResponse")
    public JAXBElement<SaveDataXmlResponse> createSaveDataXmlResponse(SaveDataXmlResponse value) {
        return new JAXBElement<SaveDataXmlResponse>(_SaveDataXmlResponse_QNAME, SaveDataXmlResponse.class, null, value);
    }

}
