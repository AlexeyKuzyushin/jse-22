package ru.rencredit.jschool.kuzyushin.tm.command.data.json.fasterXml;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Role;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Session;

public class DataJsonFasterXmlClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "data-json-fasterxml-clear";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove json file";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[REMOVE JSON FILE]");
        if (serviceLocator != null) {
            @Nullable final Session session = serviceLocator.getSessionService().getCurrentSession();
            serviceLocator.getDataEndpoint().clearDataJson(session);
            System.out.println("[OK]");
        }
        else System.out.println("[FAILED]");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }
}
