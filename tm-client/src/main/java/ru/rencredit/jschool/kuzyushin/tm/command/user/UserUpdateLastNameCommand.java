package ru.rencredit.jschool.kuzyushin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Session;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

public final class UserUpdateLastNameCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "user-update-last-name";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update user last name";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE USER LAST NAME]");
        if (serviceLocator != null) {
            @Nullable final Session session = serviceLocator.getSessionService().getCurrentSession();
            System.out.println("ENTER LAST NAME:");
            @Nullable final String lastName = TerminalUtil.nextLine();
            serviceLocator.getUserEndpoint().updateUserLastName(session, lastName);
            System.out.println("[OK]");
        }
        else System.out.println("[FAILED]");
    }
}
