package ru.rencredit.jschool.kuzyushin.tm.command.data.base64;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Role;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Session;

public final class DataBase64SaveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "data-base64-save";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to base64 file";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA BASE64 SAVE]");
        if (serviceLocator != null) {
            @Nullable final Session session = serviceLocator.getSessionService().getCurrentSession();
            serviceLocator.getDataEndpoint().saveDataBase64(session);
            System.out.println("[OK]");
        }
        else System.out.println("[FAILED]");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }
}
