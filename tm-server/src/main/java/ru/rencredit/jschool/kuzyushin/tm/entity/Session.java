package ru.rencredit.jschool.kuzyushin.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public class Session extends AbstractEntity implements Cloneable{

    @Override
    public Session clone() {
        try {
            return (Session) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    public Session() {
    }

    @Nullable
    private String userId;

    @Nullable
    private Long timestamp;

    @Nullable
    private String signature;
}
