package ru.rencredit.jschool.kuzyushin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.entity.Session;
import ru.rencredit.jschool.kuzyushin.tm.entity.User;

public interface IUserEndpoint {

    @NotNull
    User updateUserLogin(@Nullable Session session, @Nullable String login);

    @NotNull
    User updateUserPassword(@Nullable Session session, @Nullable String password);

    @Nullable
    User updateUserEmail(@Nullable Session session, @Nullable String email);

    @Nullable
    User updateUserFirstName(@Nullable Session session, @Nullable String firstName);

    @Nullable
    User updateUserLastName(@Nullable Session session, @Nullable String lastName);

    @Nullable
    User updateUserMiddleName(@Nullable Session session, @Nullable String middleName);

    @Nullable
    User viewUserProfile(@Nullable Session session);
}
