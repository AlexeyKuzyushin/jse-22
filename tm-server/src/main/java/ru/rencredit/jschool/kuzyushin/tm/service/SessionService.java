package ru.rencredit.jschool.kuzyushin.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.api.repository.ISessionRepository;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IPropertyService;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IServiceLocator;
import ru.rencredit.jschool.kuzyushin.tm.api.service.ISessionService;
import ru.rencredit.jschool.kuzyushin.tm.entity.Session;
import ru.rencredit.jschool.kuzyushin.tm.entity.User;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;
import ru.rencredit.jschool.kuzyushin.tm.exception.empty.*;
import ru.rencredit.jschool.kuzyushin.tm.exception.user.AccessDeniedException;
import ru.rencredit.jschool.kuzyushin.tm.util.HashUtil;
import ru.rencredit.jschool.kuzyushin.tm.util.SignatureUtil;

import java.util.List;

public class SessionService extends AbstractService<Session> implements ISessionService {

    private final ISessionRepository sessionRepository;

    private final IServiceLocator serviceLocator;

    public SessionService(final ISessionRepository sessionRepository,
                          final IServiceLocator serviceLocator) {
        super(sessionRepository);
        this.sessionRepository = sessionRepository;
        this.serviceLocator = serviceLocator;
    }

    @Override
    public @Nullable Session open(@Nullable String login, @Nullable String password) {
        final boolean check = checkDataAccess(login, password);
        if (!check) return null;
        final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) return null;
        final Session session = new Session();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        repository.add(session);
        return sign(session);
    }

    @Override
    public @Nullable Session sign(final Session session) {
        if (session == null) return null;
        session.setSignature(null);
        final IPropertyService propertyService = serviceLocator.getPropertyService();
        final String salt = propertyService.getSessionSalt();
        final Integer cycle = propertyService.getSessionCycle();
        final String signature = SignatureUtil.sign(session, salt, cycle);
        session.setSignature(signature);
        return session;
    }

    @Override
    public User getUser(@Nullable Session session) {
        final String userId = getUserId(session);
        return serviceLocator.getUserService().findById(userId);
    }

    @Override
    public String getUserId(@Nullable Session session) {
        validate(session);
        return session.getUserId();
    }

    @Override
    public @NotNull List<Session> getListSession(final @Nullable Session session) {
        validate(session);
        return sessionRepository.findAllSessions(session.getId());
    }

    @Override
    public void close(final @Nullable Session session) throws Exception {
        validate(session);
        sessionRepository.removeSessionByUserId(session.getUserId());
    }

    @Override
    public void closeAll(final @Nullable Session session) {
        validate(session);
        sessionRepository.clear(session.getUserId());
    }

    @Override
    @SneakyThrows
    public void validate(final @Nullable Session session) {
        if (session == null) throw new AccessDeniedException();
        if (session.getSignature() == null ||
                session.getSignature().isEmpty()) throw new AccessDeniedException();
        if (session.getUserId() == null ||
                session.getUserId().isEmpty()) throw new EmptyUserIdException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        @Nullable final Session temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @Nullable final String signatureSource = session.getSignature();
        @Nullable final String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        if (!sessionRepository.contains(session.getUserId())) throw new AccessDeniedException();
    }

    @Override
    public void validate(final @Nullable Session session, final Role role) {
        if (role == null) throw new AccessDeniedException();
        validate(session);
        final String userId = session.getUserId();
        final User user = serviceLocator.getUserService().findById(userId);
        if (user == null) throw new EmptyUserException();
        if (!role.equals(user.getRole())) throw new AccessDeniedException();
    }

    @Override
    @SneakyThrows
    public void signOutByLogin(final @Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) throw new EmptyUserException();
        final String userId = user.getId();
        sessionRepository.removeSessionByUserId(userId);
    }

    @Override
    @SneakyThrows
    public void singOutByUserId(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        sessionRepository.removeSessionByUserId(userId);
    }

    @Override
    public boolean checkDataAccess(@Nullable String login, @Nullable String password) {
        if (login == null || login.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;
        @Nullable final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) return false;
        @Nullable final String passwordHash = HashUtil.salt(password);
        if (passwordHash == null || passwordHash.isEmpty()) return false;
        return passwordHash.equals(user.getPasswordHash());

    }

    @Override
    public boolean isValid(@Nullable Session session) {
        try {
            validate(session);
            return true;
        } catch (AccessDeniedException e) {
            return false;
        }
    }
}
