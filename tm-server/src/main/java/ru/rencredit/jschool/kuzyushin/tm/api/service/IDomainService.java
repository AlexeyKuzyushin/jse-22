package ru.rencredit.jschool.kuzyushin.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.dto.Domain;

public interface IDomainService {

    void load(@Nullable Domain domain);

    void export(@Nullable Domain domain);
}
