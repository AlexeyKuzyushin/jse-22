package ru.rencredit.jschool.kuzyushin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.api.endpoint.IUserEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IServiceLocator;
import ru.rencredit.jschool.kuzyushin.tm.entity.Session;
import ru.rencredit.jschool.kuzyushin.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public final class UserEndpoint implements IUserEndpoint {

    private IServiceLocator serviceLocator;

    public UserEndpoint() {
    }

    public UserEndpoint(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    @Override
    @WebMethod
    public User updateUserLogin(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "login", partName = "login") @Nullable String login) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().updateLogin(session.getUserId(), login);
    }

    @NotNull
    @Override
    @WebMethod
    public User updateUserPassword(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "password", partName = "password") @Nullable String password) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().updatePassword(session.getUserId(), password);
    }

    @Nullable
    @Override
    @WebMethod
    public User updateUserEmail(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "email", partName = "email") @Nullable String email) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().updateEmail(session.getUserId(), email);
    }

    @Nullable
    @Override
    @WebMethod
    public User updateUserFirstName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "firstName", partName = "firstName") @Nullable String firstName) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().updateFirstName(session.getUserId(), firstName);
    }

    @Nullable
    @Override
    @WebMethod
    public User updateUserLastName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "lastName", partName = "lastName") @Nullable String lastName) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().updateLastName(session.getUserId(), lastName);
    }

    @Nullable
    @Override
    @WebMethod
    public User updateUserMiddleName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "middleName", partName = "middleName") @Nullable String middleName) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().updateMiddleName(session.getUserId(), middleName);
    }

    @Nullable
    @Override
    @WebMethod
    public User viewUserProfile(
            @WebParam(name = "session", partName = "session") @Nullable final Session session) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getSessionService().getUser(session);
    }
}
