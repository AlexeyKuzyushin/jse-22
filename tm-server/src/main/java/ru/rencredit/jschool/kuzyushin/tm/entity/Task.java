package ru.rencredit.jschool.kuzyushin.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class Task extends AbstractEntity {

    public static final long serialVersionUID = 1;

    @NotNull
    private String name;

    @Nullable
    private String description;

    @NotNull
    private String userId;

    @Override
    public String toString() {
        return getId() + ": " + name;
    }
}
