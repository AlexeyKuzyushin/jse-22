package ru.rencredit.jschool.kuzyushin.tm.dto;

public class Fail extends Result {

    public Fail() {
        success = false;
        message = "";
    }
    public Fail(final Exception e) {
        success = false;
        if (e == null) return;
        message = e.getMessage();
    }
}
